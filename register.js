
var emailglobal ='';
var glotp ='';
const urlParams = new URLSearchParams(window.location.search);
var refcode = urlParams.get('r');
console.log(refcode);
$('#referral_code').val(refcode)
const spinner = $('.waiting');
$(document).ready(function() {
    $('#staticBackdrop').modal('show');
    $("#form2").hide();
    $("#form3").hide();
    $("#form4").hide();
    $(".resentotp").hide();
    $(".nextbutton").show();
    $(".cdotp").show();
    $(".nextform1").show();
    $('#myForm').validate({
      errorElement: 'span', // Use a span element for the error message
      rules: {
        name: {
          required: true
        },
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        name: {
          required: "Please enter your name"
        },
        email: {
          required: "Please enter your email",
          email: "Please enter a valid email address"
        }
      },
      errorPlacement: function(error, element) {
        error.appendTo(element.next('.error-message')); // Append the error message to the next .error-message span
      },
      highlight: function(element, errorClass) {
        $(element).addClass(errorClass); // Add the error class to the form element
        $(element).next('.error-message').show(); // Show the error message next to the form element
      },
      unhighlight: function(element, errorClass) {
        $(element).removeClass(errorClass); // Remove the error class from the form element
        $(element).next('.error-message').hide(); // Hide the error message next to the form element
      },
      submitHandler: function(form) {
        // handle form submission
      }
    });
    $('#form1').validate({
      errorElement: 'span', // Use a span element for the error message
      rules: {
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        email: {
          required: "Please enter your email",
          email: "Please enter a valid email address"
        }
      },
      errorPlacement: function(error, element) {
        error.appendTo(element.next('.error-message')); // Append the error message to the next .error-message span
      },
      highlight: function(element, errorClass) {
        $(element).addClass(errorClass); // Add the error class to the form element
        $(element).next('.error-message').show(); // Show the error message next to the form element
      },
      unhighlight: function(element, errorClass) {
        $(element).removeClass(errorClass); // Remove the error class from the form element
        $(element).next('.error-message').hide(); // Hide the error message next to the form element
      },
      submitHandler: function(form) {
        $(".nextbutton").show();
        $(".cdotp").show();
        $('.resentotp').hide();
        emailglobal = $('#email').val()
        var settings = {
          "url": "https://production-api.fantasiachain.com/1.0/auth/initialization",
          "method": "POST",
          "timeout": 0,
          "headers": {
            "Content-Type": "application/json"
          },
          "data": JSON.stringify({
            "emailAddress": emailglobal
          }),
          beforeSend: function() {
            spinner.show();
            $(".nextform1").hide();
          },
          success: function(response) {
            spinner.hide();
            $(".nextform1").show();
          },
          error: function(error) {
            spinner.hide();
          }
        }; 

// // Make the API request
// $.ajax({
//   url: 'https://example.com/api',
//   method: 'GET',
//   dataType: 'json',

// });


        $.ajax(settings).done(function (response) {
          console.log(response);
          if(response.error==0)
          {
            // var count = 300;
            // var minutes, seconds;
            // var countdown = setInterval(function() {
            //   minutes = parseInt(count / 60, 10);
            //   seconds = parseInt(count % 60, 10);
            //   minutes = minutes < 10 ? "0" + minutes : minutes;
            //   seconds = seconds < 10 ? "0" + seconds : seconds;
            //   $('#countdown').text(minutes + ":" + seconds);
            //   count--;
            //   if (count < 0) {
            //     clearInterval(countdown);
            //     alert('Countdown finished!');
            //   }
            // }, 1000);


             var endTime = new Date(Date.parse(new Date()) + 5 * 60 * 1000); // 5 minutes from now
              var countdownElement = $('#countdown');

              function updateCountdown() {
                var now = new Date();
                var timeLeft = Math.max(0, endTime - now);
                var minutesLeft = Math.floor(timeLeft / 1000 / 60);
                var secondsLeft = Math.floor((timeLeft / 1000) % 60);

                // Update countdown element
                countdownElement.text(minutesLeft + ':' + (secondsLeft < 10 ? '0' : '') + secondsLeft);

                if (timeLeft > 0) {
                  requestAnimationFrame(updateCountdown);
                } else {
                  // Countdown has ended
                  countdownElement.text('00:00');
                  $(".resentotp").show();
                  $(".cdotp").hide();
                  $(".nextbutton").hide();
                  
                }
              }

              // Initial call to update countdown
              updateCountdown();

            $("#form1").hide();
            $("#form2").show();
          }
          else
          {
            alert(response.message)
          }
        });
      }
    });
    $('#form2').submit(function(e) {
        e.preventDefault();
        
        const otp1 = $('#otp1').val()
        const otp2 = $('#otp2').val()
        const otp3 = $('#otp3').val()
        const otp4 = $('#otp4').val()
        const otp5 = $('#otp5').val()
        const otp6 = $('#otp6').val()
        glotp = `${otp1}${otp2}${otp3}${otp4}${otp5}${otp6}`
        var settings = {
          "url": "https://production-api.fantasiachain.com/1.0/auth/registration/otp/verify",
          "method": "POST",
          "timeout": 0,
          "headers": {
            "Content-Type": "application/json"
          },
          "data": JSON.stringify({
            "emailAddress": emailglobal,
            "otp": glotp
          }),
        };
        
        $.ajax(settings).done(function (response) {
          console.log(response);
          if(response.data==true)
          {
            $("#form1").hide();
            $("#form2").hide();
            $("#form3").show();
          }
          else
          {   
            $('.opt-error-message').show()
            setTimeout(() => {
              $('.opt-error-message').hide()
            }, 2000);
          }

        });


      }); 
    $('#form3').submit(function(e) {
        var password = $('#password').val();
        var confirmPassword = $('#confirm_password').val();
        if(password=="")
        {
            $('#password-error').html('Passwords not empty');
            e.preventDefault();
        }
        else
        {
            $('#password-error').html('');
            if (password != confirmPassword) {
              $('#confirm_password_error').html('Passwords do not match.');
              e.preventDefault(); // prevent form submission
            } else {
              $('#confirm_password_error').html('');
              var settings = {
                "url": "https://production-api.fantasiachain.com/1.0/auth/registration",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "emailAddress": emailglobal,
                  "otp": glotp,
                  "password": password,
                  "rePassword": confirmPassword,
                  "referrerCode": $('#referral_code').val()
                }),
              };
              
              $.ajax(settings).done(function (response) {
                console.log(response);
                if(response.error==0)
                {
                $("#form1").hide();
                $("#form2").hide();
                $("#form3").hide();
                $("#form4").show();
                }
                else
                {
                  alert(response.message)
                }
              });

            }
        }
      });
    $('.otp-input').on('keyup', function() {
        var $this = $(this);
        if ($this.val()) {
          $this.next().focus();
        } else if ($this.prev().val()) {
          $this.prev().focus();
        }
      });
    $('.resentotp').click(function() {
      $("#form1").show();
      $("#form2").hide();
      $("#form3").hide();
      $("#form4").hide();
    });
  });